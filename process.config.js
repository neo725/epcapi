module.exports = {
	apps: [
		{
			name		: "epc-api",
			script		: "./server.js",
			instances	: 1,
			exec_mode	: "cluster",
			watch		: true,
			env: {
				"PORT": 3010,
				"NODE_ENV": "development",
			},
			env_production: {
				"PORT": 80,
				"NODE_ENV": "production",
			}
		}
	]
}