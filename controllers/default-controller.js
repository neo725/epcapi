/* jshint -W097 */
'use strict';
let colors = require('colors')
let snakeCaseKeys = require('snakecase-keys')
let store = require('../stores/default-store')

let controller = function() {
    
    return {
        hello: function(req, res) {
            console.log('run hello'.yellow)

            res.json({ message: 'hello world' })
        },

        test: function(req, res) {
            console.log('run test'.yellow)

            store.test()

            res.json({ message: 'test' })
        },

        getBranches: function(req, res) {
            console.log('run getBranches'.yellow)

            store.getBranches((err, result) => {
                if (err) {
                    console.error(err)

                    return res.json({ 'status': 'error' })
                }

                result = snakeCaseKeys(result)

                console.log('result get :'.green)
                console.log(result)
                res.json(result)
            })
        },

        addBranch: function(req, res) {
            console.log('run addBranches'.yellow)

            let branch = req.body

            store.addBranch(branch, (err, result) => {
                if (err) {
                    console.error(err)
                    return res.json({ 'status': 'error' })
                }
                
                result = snakeCaseKeys(result)
                
                console.log('result get :'.green)
                console.log(result)

                res.json(result)
            })
        },

        getReaders: function(req, res) {
            let branch_code = req.params.branch_code

            store.getReaders(branch_code, (err, result) => {
                if (err) {
                    console.error(err)

                    return res.json({ 'status': 'error' })
                }
                
                result = snakeCaseKeys(result)
                
                console.log('result get :'.green)
                console.log(result)

                res.json(result)
            })
        },

        addReader: function(req, res) {
            let branch_code = req.params.branch_code
            let reader = req.body

            store.addReader(branch_code, reader, (err, result) => {
                if (err) {
                    console.error(err)

                    return res.json({ 'status': 'error' })
                }
                
                result = snakeCaseKeys(result)
                
                console.log('result get :'.green)
                console.log(result)

                res.json(result)
            })
        },

        getEpcs: function(req, res) {
            let branch_code = req.params.branch_code
            let reader_id = req.params.reader_id

            store.getEpcs(branch_code, reader_id, (err, result) => {
                if (err) {
                    console.error(err)

                    return res.json({ 'status': 'error' })
                }
                
                result = snakeCaseKeys(result)
                
                console.log('result get :'.green)
                console.log(result)

                res.json(result)
            })
        },

        addEpc: function(req, res) {
            let branch_code = req.params.branch_code
            let reader_id = req.params.reader_id
            let payload = req.body

            store.addEpc(branch_code, reader_id, payload, (err, result) => {
                if (err) {
                    console.error(err)

                    return res.json({ 'status': 'error' })
                }
                
                result = snakeCaseKeys(result)
                
                console.log('result get :'.green)
                console.log(result)

                res.json(result)
            })
        },

        addEpcByMacAddress: function(req, res) {
            let payload = req.body

            let inputs = Array.isArray(payload) ? [...payload] : [payload]

            console.log(`inputs.length = ${inputs.length}`.yellow)

            const runAddEpcPromise = (data) => {
                return new Promise((resolve, reject) => {
                    // console.log('data :'.yellow)
                    // console.log(data)

                    let mac_address = data['mac_address']
                    let epc = data['epc']
                    let reader_time = data['reader_time']

                    store.addEpcByMacAddress(mac_address, epc, reader_time, (err, result) => {
                        // console.log('callback in !!!'.bgGreen.white)

                        if (err) {
                            console.error(err)

                            // res.status(400)

                            // return res.json({ 'status': 'error', 'message': err.message })
                            //results.push({ 'code': 400, 'status': 'error', 'message': err.message })
                            let data = { 'code': 400, 'status': 'error', 'message': err.message }
                            return resolve(data)
                        }

                        result = snakeCaseKeys(result)
                        
                        // console.log('result get :'.green)
                        // console.log(result)

                        //res.json(result)
                        //results.push(result[0])
                        resolve(result[0])
                    })
                })
            }

            const asyncForEach = async (array, callback) => {
                for (let index = 0; index < array.length; index++) {
                    await callback(array[index], index, array)
                }
            }

            const runAddAsync = async (inputs) => {
                let results = []
                const start = async (inputs) => {
                    return await asyncForEach(inputs, async (data) => {
                        let result = await runAddEpcPromise(data)
                        // console.log('await back...'.rainbow)
                        // console.log(result)
    
                        results.push(result)
                    })
                }

                //console.log('before start()...'.bgMagenta)
                await start(inputs)
                //console.log('after start()...'.bgMagenta)

                return results
            }
            
            runAddAsync(inputs).then(result => {
                console.log('runAddAsync result ...'.america)
                console.log(result)

                res.json(result)
            }).catch(error => {
                console.log('runAddAsync catch...'.bgRed.white)
                console.log(error)

                res.status(400)
                res.json(error)
            })
        }
    }
}

module.exports = controller()