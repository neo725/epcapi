/* jshint -W097 */
'use strict';

let controller = require('../controllers/default-controller')

module.exports = function(app) {

    app.route('/')
        .get(controller.hello)

    app.route('/test')
        .get(controller.test)

    app.route('/branches')
        .get(controller.getBranches)
        .post(controller.addBranch)

    app.route('/branches/:branch_code/readers')
        .get(controller.getReaders)
        .post(controller.addReader)
    
    app.route('/branches/:branch_code/readers/:reader_id')
        .get(controller.getEpcs)
        .post(controller.addEpc)

    app.route('/epc')
        .post(controller.addEpcByMacAddress)
}