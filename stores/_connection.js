/* jshint -W097 */
'use strict';

let colors = require('colors')
let config = require('../config')

let conf = config.db

let db_config = {
    user: conf.user,
    password: conf.password,
    server: conf.server,
    database: conf.database,
    options: {
        encrypt: true
    },
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    }
}

module.exports = function(sql) {
    
    return new Promise((resolve, reject) => {
        // sql.connect(db_config).then(pool => {
        //     console.log(' << mssql server connected ! >> '.bgYellow.black)

        //     resolve(pool)
        // }).catch(err => {
        //     reject(err)
        // })
        
        new sql.connect(db_config)
            .then(pool => {
                console.log(' << mssql server connected ! >> '.bgYellow.black)

                resolve(pool)
            })
            .catch(err => {
                console.error(' << mssql server connected, but error happen ! >> '.bgRed.white)
                console.log(err)

                reject(err)
            })

        // pool.connect(err => {
        //     if (err) {
        //         console.error(' << mssql server connected, but error happen ! >> '.bgRed.white)
        //         console.log(err)

        //         return reject(err)
        //     }

        //     console.log(' << mssql server connected ! >> '.bgYellow.black)
        //     resolve(pool)
        // })
            
            // .then(pool => {
            //     resolve(pool)
            // })
            // .catch(err => {
            //     reject(err)
            // })
    })
    
    // var connection = new sql.Connection(db_config)

    // return new Promise(function(resolve, reject) {
    //     connection.connect().then(function () {
    //         console.log(' << mssql server connected ! >> '.bgYellow.black)
    
    //         let _request = new sql.Request(connection)

    //         resolve(_request)

    //     }).catch(function(err) {
    //         reject(err)
    //     })
    // })
}