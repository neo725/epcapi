/* jshint -W097 */
'use strict';

let sql = require('mssql')
let connection = require('./_connection')

let util = (() => {
    let parseSymbolForMacAddress = (mac_address) => {
        return mac_address.replace(/(-|:)/ig, '').toUpperCase()
    }

    return {
        MacAddress: {
            Parse: parseSymbolForMacAddress
        }
    }
})()

let store = function () {

    let test = function () {
        connection(sql).then(pool => {
            new sql.Request(pool)
                .query('SELECT * FROM Reader')
                .then(result => {
                    console.log(result)
                })
        })
    }

    let getBranches = function(callback) {
        connection(sql).then(pool => {
            new sql.Request(pool)
                .query('select * from [Branch]')
                .then(result => {
                    callback(undefined, result)
                })
                .catch(err => {
                    callback(err)
                })
        })
    }

    let addBranch = function(branch, callback) {
        
        connection(sql).then(pool => {
            let sqlStr = `
insert into [Branch]
(BranchName, Memo, CreateFrom)
values
(@branch_name, @memo, @create_from)

select BranchId, BranchCode
from [Branch]
where BranchId = SCOPE_IDENTITY()
`

            new sql.Request(pool)
                //.input('branch_code', sql.VarChar, branch.branch_code)
                .input('branch_name', sql.NVarChar(50), branch.name)
                .input('memo', sql.NVarChar(500), branch.memo)
                .input('create_from', sql.VarChar(50), 'api')
                .query(sqlStr)
                .then(result => {
                    callback(null, result)
                })
                .catch(err => {
                    callback(err)
                })
        })
    }

    let getReaders = function(branch_code, callback) {
        connection(sql).then(pool => {
            let sqlStr = `
select r.*
from [Reader] r
inner join [Branch] b on b.BranchId = r.BranchId
where b.BranchCode = @branch_code
`
            new sql.Request(pool)
                .input('branch_code', sql.VarChar(36), branch_code)
                .query(sqlStr)
                .then(result => {
                    callback(null, result)
                })
                .catch(err => {
                    callback(err)
                })
        })
    }

    let addReader = function(branch_code, reader, callback) {
        connection(sql).then(pool => {
            let sqlStr = `
insert into [Reader]
(MacAddress, ReaderName, Memo, BranchId, CreateFrom)
select @mac_address, @name, @memo, BranchId, @create_from
from [Branch]
where BranchCode = @branch_code

select *
from [Reader]
where ReaderId = SCOPE_IDENTITY()
            `

            new sql.Request(pool)
                //.input('mac_address', sql.VarChar(12), reader.mac_address.replace(/(-|:)/ig, '').toUpperCase())
                .input('mac_address', sql.VarChar(12), util.MacAddress.Parse(reader.mac_address))
                .input('name', sql.NVarChar(50), reader.name)
                .input('memo', sql.NVarChar(500), reader.memo)
                .input('branch_code', sql.VarChar(36), branch_code)
                .input('create_from', sql.VarChar(50), 'api')
                .query(sqlStr)
                .then(result => {
                    callback(null, result)
                })
                .catch(err => {
                    callback(err)
                })
        })
    }

    let getEpcs = function(branch_code, reader_id, callback) {
        connection(sql).then(pool => {
            let sqlStr = `
select e.*, r.ReaderName, r.MacAddress, b.BranchId, b.BranchName
from [EpcData] e
inner join [Reader] r on r.ReaderId = e.ReaderId
inner join [Branch] b on b.BranchId = b.BranchId
where r.ReaderId = @reader_id
    and b.BranchCode = @branch_code
`
            
            new sql.Request(pool)
                .input('branch_code', sql.VarChar(36), branch_code)
                .input('reader_id', sql.Int, reader_id)
                .query(sqlStr)
                .then(result => {
                    callback(null, result)
                })
                .catch(err => {
                    callback(err)
                })
        })
    }

    let getBranchByReaderId = function(reader_id, callback) {
        connection(sql).then(pool => {
            let sqlStr = `
select b.*
from [Reader] r
inner join [Branch] b on b.BranchId = r.BranchId
where r.ReaderId = @reader_id
`
            new sql.Request(pool)
                .input('reader_id', sql.Int, reader_id)
                .query(sqlStr)
                .then(result => {
                    if (result.length == 0) {
                        return callback({ status: -1, message: `Branch not mapping with ReaderId [${reader_id}]` })
                    }

                    callback(null, result[0])
                })
                .catch(err => {
                    callback(err)
                })
        })
    }

    let getReaderByMacAddress = function(mac_address, callback) {
        connection(sql).then(pool => {
            let sqlStr = `
select *
from [Reader] r
where r.MacAddress = @mac_address
`
            // console.log('Here is getReaderByMacAddress !!'.bgCyan.black)
            // console.log(sqlStr)
            // console.log('.')
            new sql.Request(pool)
                .input('mac_address', sql.VarChar(12), util.MacAddress.Parse(mac_address))
                .query(sqlStr)
                .then(result => {
                    if (result.length == 0) {
                        return callback({ status: -1, message: `No reader mapping to mac address : [${mac_address}]` })
                    }

                    callback(null, result[0])
                })
                .catch(err => {
                    callback(err)
                })
        })
    }

    let addEpcByMacAddress = function(mac_address, epc, reader_time, callback) {
        // console.log('Here is addEpcByMacAddress !!'.bgBlue.white)
        // console.log(mac_address)
        // console.log(epc)
        // console.log(reader_time)
        getReaderByMacAddress(mac_address, (err, reader) => {
            // console.log('getReaderByMacAddress back ...'.bgBlue.white)
            // console.log(err)
            // console.log(reader)

            if (err) {
                return callback({ status: -1, message: err.message })
            }

            getBranchByReaderId(reader.ReaderId, (err, branch) => {
                // console.log('getBranchByReaderId back ...'.bgBlue.white)
                // console.log(err)
                // console.log(branch)

                if (err) {
                    return callback({ status: -2, message: err.message })
                }

                addEpc(branch.BranchCode, reader.ReaderId, { 'epc': epc , 'reader_time': reader_time }, callback)
            })
        })
    }

    let addEpc = function(branch_code, reader_id, epc, callback) {
        connection(sql).then(pool => {
            let sqlStr = `
insert into [EpcData]
(Epc, ReaderId, ReaderTime)
select @epc_code, r.ReaderId, @reader_time
from [Reader] r
inner join [Branch] b on b.BranchId = r.BranchId
where r.ReaderId = @reader_id
    and b.BranchCode = @branch_code

select *
from [EpcData]
where EpcId = SCOPE_IDENTITY()
`
            // console.log('here is sql :'.bgBlue.white)
            // console.log(sqlStr)

            new sql.Request(pool)
                .input('branch_code', sql.VarChar(36), branch_code)
                .input('reader_id', sql.Int, reader_id)
                .input('epc_code', sql.VarChar(24), epc.epc)
                .input('reader_time', sql.VarChar, epc.reader_time)
                .query(sqlStr)
                .then(result => {
                    callback(null, result)
                })
                .catch(err => {
                    callback(err)
                })
        })
    }

    return {
        test: test,
        getBranches: getBranches,
        addBranch: addBranch,
        getReaders: getReaders,
        addReader: addReader,
        getEpcs: getEpcs,
        addEpc: addEpc,
        addEpcByMacAddress: addEpcByMacAddress
    }
}

module.exports = store()