/* jshint -W097 */
'use strict';

let express = require('express'),
    cors = require('cors'),
    app = express(),
    server = require('http').createServer(app),
    port = process.env.PORT || process.env.NODE_ENV == 'production' ? 3015 : 3010,
    bodyParser = require('body-parser'),
    colors = require('colors')

// initialize setting
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// initialize route mapping to which controller
var route = require('./routes/default-route')
route(app)

// set server port
server.listen(port)

//scan()

console.log(` epc-api server run on : ` + ` ${port} `.bgGreen.black)