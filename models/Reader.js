/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Reader', {
		ReaderId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		MacAddress: {
			type: DataTypes.CHAR,
			allowNull: false
		},
		ReaderName: {
			type: DataTypes.STRING,
			allowNull: true
		},
		Memo: {
			type: DataTypes.STRING,
			allowNull: true
		},
		BranchId: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
		CreateDate: {
			type: DataTypes.DATE,
			allowNull: false
		},
		CreateFrom: {
			type: DataTypes.STRING,
			allowNull: false
		}
	}, {
		tableName: 'Reader'
	});
};
