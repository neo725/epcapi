/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('Branch', {
		BranchId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		BranchCode: {
			type: DataTypes.UUIDV4,
			allowNull: false
		},
		BranchName: {
			type: DataTypes.STRING,
			allowNull: false
		},
		Memo: {
			type: DataTypes.STRING,
			allowNull: true
		},
		CreateDate: {
			type: DataTypes.DATE,
			allowNull: false
		},
		CreateFrom: {
			type: DataTypes.STRING,
			allowNull: false
		}
	}, {
		tableName: 'Branch'
	});
};
