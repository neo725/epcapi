/* jshint -W097 */
'use strict';

/* please update these 3 settings into your environment */
const db_server = "epc-azure.database.windows.net"
const db_user_service = "epc_service_operator"
const db_password_service = 't2e6roDo9agAblphl1pe'

/* do not touch those lines below */
const db_server_full = `tcp:${db_server},1433`
const db_catalog = "epc"

module.exports = function() {
    return {
        "db": {
            "server": db_server,
            "server_full": db_server_full,
            "database": db_catalog,
            "user": db_user_service,
            "password": db_password_service,
            "connection_string": `Server=${db_server_full};Initial Catalog=${db_catalog};Persist Security Info=False;User ID=${db_user_service};Password=${db_password_service};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;`
        },
    }
}()